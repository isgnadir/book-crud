## Setup

## Run Environment in docker

```bash
docker-compose up -d 
```
This will init Spring Boot backend on port 8080, React Frontend on 3000, and MySql database on 3306.

## Run backend 

```shell
cd api-backend
```

### Build jar
```bash
./gradlew build
```

### Setup environment variables
Following environment variables should be set
* `SPRING_DATASOURCE_USERNAME`
* `SPRING_DATASOURCE_PASSWORD`
* `SPRING_DATASOURCE_URL`

Example with .env file:
```shell
SPRING_DATASOURCE_USERNAME=user
SPRING_DATASOURCE_PASSWORD=password
SPRING_DATASOURCE_URL=jdbc:mysql://localhost:3306/db
```
```bash
export $(cat .env | xargs)
```

### Run Spring app
```bash
./gradlew bootRun
```

## Run Frontend

```shell
cd frontend
```

```shell
npm install
```

```shell
npm start
```

## Running infrastructure configuration

```shell
cd ansible
```

Define hosts.yml file with your hosts

```yaml
db_servers:
  hosts:
    3.33.3.33:

web_apps:
  hosts:
    55.5.5.5:
```

Run nginx, mysql and docker setup
```shell
ansible-playbook infra-ply.yml
```

Run frontend and backend setup
```shell
ansible-playbook frontend-ply.yml
```

```shell
ansible-playbook backend-ply.yml
```