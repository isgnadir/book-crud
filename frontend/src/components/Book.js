import axios from "axios";
import $ from "jquery";
import {useEffect, useState} from "react";

export const Book = () => {

    axios.defaults.baseURL = process.env.REACT_APP_API_URL;
    axios.defaults.withCredentials = true;

    const [bookList, setBookList] = useState([]);
    const [load, setLoad] = useState(false);

    useEffect(() => {
        if(!load) {
            const onPageLoad = async () => {
                try {
                    const {data} = await axios.get('/books');
                    console.log("loaded book list", data);
                    setBookList(data.books);
                    setLoad(true);
                } catch (e) {
                    //axiosErrorHandler(e.response);
                }
            }
            if (document.readyState === 'complete') {
                onPageLoad();
            } else {
                window.addEventListener('load', onPageLoad);
                return () => window.removeEventListener('load', onPageLoad);
            }
        }
    })

    const onCreate = () => {
        let title = $('#fc-title').val();
        let author = $("#fc-author").val();
        try {
            axios.post('/books', {title, author}, {withCredentials: true});
            window.location.reload();
        } catch (e) {
            // error
        }
    }

    return (
        <div className="container">
            <div className="modal fade" id="createBookModal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content row">
                        <div className="modal-header">
                            <h3 className="modal-title" id="exampleModalLabel">Create</h3>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form onSubmit={onCreate}  noValidate className={"col-md-12"}>
                                <div className="form-group">
                                    <label htmlFor="name">Title</label>
                                    <input type="text"
                                           className="form-control"
                                           name="title"
                                           id="fc-title"
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="description">Author</label>
                                    <input type="text"
                                           className="form-control"
                                           name="author"
                                           id="fc-author"
                                    />
                                </div>
                                <br/>
                                <div className="form-group">
                                    <button type="submit" className="btn btn-primary btn-block">Submit</button>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary"
                                    data-dismiss="modal">Close
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal fade" id="editBookModal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content row">
                        <div className="modal-header">
                            <h3 className="modal-title" id="exampleModalLabel">Edit</h3>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form  noValidate className={"col-md-12"}>
                                <div className="form-group">
                                    <label htmlFor="name">Title</label>
                                    <input type="text"
                                           className="form-control"
                                           name="name"
                                           id="fc-name"
                                           // value={currentCourseName}
                                           // onChange={(e) => setCurrentCourseName(e.target.value)}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="description">Author</label>
                                    <input type="text"
                                           className="form-control"
                                           name="description"
                                           id="fc-description"
                                           // value={currentCourseDescription}
                                           // onChange={(e) => setCurrentCourseDescription(e.target.value)}
                                    />
                                </div>
                                <br/>
                                <div className="form-group">
                                    <button type="submit" className="btn btn-primary btn-block">Submit</button>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary"
                                    data-dismiss="modal">Close
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <h1 className="h3 mb-3 font-weight-normal text-center">My Books</h1>
            <button className="btn btn-info" type="button" data-toggle="modal" data-target="#createBookModal">Create</button>
            <br/> <br/>
            <table className="table table-dark table-striped">
                <thead className="table-dark">
                <tr className="">
                    <th>Book Title</th>
                    <th>Book Author</th>
                    <th>Control</th>
                </tr>
                </thead>
                <tbody>
                {bookList.map((book) => (
                    <tr key={book.id}>
                        <td>{book.title}</td>
                        <td>{book.author}</td>
                        <td>
                            <button className="btn btn-warning" data-toggle="modal" data-target="#editBookModal"
                                    // onClick={() => {
                                    //     setCurrentCourseId(course.id)
                                    //     setCurrentCourseName(course.name)
                                    //     setCurrentCourseDescription(course.description)}}
                            >
                                Edit
                            </button>
                            &nbsp;
                            <button className="btn btn-danger">Delete</button>
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>

        </div>
    );
}