import { BrowserRouter, Route, Routes } from "react-router-dom";
import './App.css';
import {Book} from "./components/Book";

function App() {
  return (
      <BrowserRouter>
        <div >
          <Routes>
            <Route path={"/"} element={<Book/>}/>
          </Routes>
        </div>
      </BrowserRouter>
  );
}

export default App;
