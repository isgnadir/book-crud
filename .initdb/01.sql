DROP TABLE IF EXISTS book;

CREATE TABLE book
(
    id        INT          NOT NULL AUTO_INCREMENT,
    title     VARCHAR(255) NOT NULL,
    author   VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO book (title, author) VALUES ('The Hobbit', 'J.R.R. Tolkien');
INSERT INTO book (title, author) VALUES ('The Fellowship of the Ring', 'J.R.R. Tolkien');