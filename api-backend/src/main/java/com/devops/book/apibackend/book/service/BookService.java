package com.devops.book.apibackend.book.service;

import com.devops.book.apibackend.book.domain.Book;
import com.devops.book.apibackend.book.dto.BookResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.simple.JdbcClient;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookService {

    private final JdbcClient jdbcClient;

    public Book getById(Long id) {
        log.info("getById: {}", id);
        return jdbcClient.sql(Query.GET_BY_ID)
                .param(id)
                .query(Book.class)
                .list().stream()
                .findFirst()
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Book not found"));
    }

    public BookResponse getAll() {
        log.info("getAll");
        var books = jdbcClient.sql(Query.GET_ALL)
                .query(Book.class)
                .list();
        return BookResponse.of(books);
    }

    public void create(Book book) {
        log.info("create: {}", book);
        jdbcClient.sql(Query.CREATE)
                .params(book.getAuthor(), book.getTitle())
                .update();
    }

}
