package com.devops.book.apibackend.book.service;

public final class Query {

    public static final String GET_ALL = """
            SELECT id,
                   author,
                   title
              FROM book
        """;

    public static final String GET_BY_ID = """
            SELECT id,
                   author,
                   title
              FROM book
             WHERE id = ?
            """;

    public static final String CREATE = """
            INSERT INTO book (author, title)
                 VALUES (?, ?)
            """;

    private Query() {
    }

}
