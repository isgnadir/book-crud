package com.devops.book.apibackend.book.dto;

import com.devops.book.apibackend.book.domain.Book;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class BookResponse {

    private List<Book> books;

}
