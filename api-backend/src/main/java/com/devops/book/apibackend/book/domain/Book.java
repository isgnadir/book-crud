package com.devops.book.apibackend.book.domain;

import lombok.Data;

@Data
public class Book {

    private Long id;
    private String title;
    private String author;

}
