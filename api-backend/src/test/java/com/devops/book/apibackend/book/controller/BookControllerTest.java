package com.devops.book.apibackend.book.controller;

import com.devops.book.apibackend.book.domain.Book;
import com.devops.book.apibackend.book.service.BookService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BookController.class)
public class BookControllerTest {

    public static final String BOOKS_PATH = "/books";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookService bookService;

    @Test
    void getById_Success() throws Exception {
        //given
        var given = 1L;

        var expected = new Book();
        expected.setId(1L);
        expected.setTitle("Test");
        expected.setAuthor("Test");

        //when
        when(bookService.getById(given)).thenReturn(expected);

        //then
        mockMvc.perform(get(BOOKS_PATH + "/" + given))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(expected.getId()))
                .andExpect(jsonPath("$.title").value(expected.getTitle()))
                .andExpect(jsonPath("$.author").value(expected.getAuthor()));
    }
}
